#!/usr/bin/env python
import irc.bot
import json
import random
import re


class PyIRCDiceBot(irc.bot.SingleServerIRCBot):
    """
    PyIRCDiceBot functions as a typical IRC user. Additionally, it listens for
    messages pertaining to dice rolls, then rolls and reports the individual
    roll of each die as well as the total result.

    The only public command the bot understands is dice notation.
    NdP + QdR + ... + YdZ
    N, P, Q, R, Y, Z: An integer above zero.
    + : Can represent either + or -

    When the bot detects a message with dice notation anywhere in that message,
    it will roll the appropriate dice, show the result of each individual die
    in a list, and show the total result.

    The bot understands four private commands:
    - the aforementioned dice rolling command
    - a 'cometo' command to pull a bot into a channel
    - a 'goaway' command to push a bot out of a channel
    - a 'name' command to change the bot's name
    """
    # dice_notation = re.compile(r'((\+|-)?(\d+)(d\d+)?)')
    # dice_notation = re.compile(r'(\d+#)?(\+|-)?(\d+)(d\d+)?')
    dice_notation = re.compile(r'(\+|-)?(\d*#*\d+)(d\d+)?')
    join_commands = ["join", "cometo"]
    leave_commands = ["leave", "goaway"]
    name_commands = ["name", "nick", "nickname"]

    def __init__(self, nickname, realname, server, port=6667, password=None,
                 default_channels=[]):
        irc.bot.SingleServerIRCBot.__init__(
            self,
            [irc.bot.ServerSpec(server, port, password)],
            nickname,
            realname
        )
        self.default_channels = default_channels

    def on_nicknameinuse(self, connection, event):
        connection.nick(connection.get_nickname() + "_")

    def on_welcome(self, connection, event):
        print("Connection successfully established.")
        for channel in self.default_channels:
            connection.join(channel)

    def on_pubmsg(self, connection, event):
        message = event.arguments[0]
        split_message = message.split(" ")
        description = " ".join(split_message[1:])
        matches = self.dice_notation.findall(split_message[0])
        if matches:
            reply = self.roll_dice(matches, message, event.source.nick)
            # We need a try/except block here to prevent overly long messages
            # from breaking the bot
            if reply:
                try:
                    connection.privmsg(event.target, reply)
                except Exception as e:
                    print(e)

    def on_privmsg(self, connection, event):
        message = event.arguments[0]
        split_message = message.split(" ")
        description = " ".join(split_message[1:])
        matches = self.dice_notation.findall(split_message[0])
        if matches:
            reply = self.roll_dice(matches, message, event.source.nick)
            # We need a try/except block here to prevent overly long messages
            # from breaking the bot
            if reply:
                try:
                    connection.privmsg(event.source.nick, reply)
                except Exception as e:
                    print(e)
        else:
            # Check to see if the bot is being summoned or told to leave
            if self.message_matches_type(message, self.join_commands):
                arguments = message.split(" ")
                channel = arguments[1]
                if not channel.startswith("#"):
                    channel = "#%s" % channel
                try:
                    connection.join(channel)
                except Exception as e:
                    print(e)
            if self.message_matches_type(message, self.leave_commands):
                arguments = message.split(" ")
                channel = arguments[1]
                if not channel.startswith("#"):
                    channel = "#%s" % channel
                try:
                    connection.part(channel)
                except Exception as e:
                    print(e)
            # Check if someone is trying to rename the bot
            if self.message_matches_type(message, self.name_commands):
                arguments = message.split(" ")
                name = arguments[1]
                if len(name) > 32:
                    name = name[:32]
                connection.nick(name)

    def message_matches_type(self, message, type_list):
        for command in type_list:
            if message.lower().startswith(command):
                return True
        return False

    def on_dccmsg(self, connection, event):
        print("DCC message: " + event.arguments[0])

    def roll_dice(self, matches, message, nickname):
        """
        Match group anatomy:
        match[0]: +, -, or None
        match[1]: integer or two integers separated by #
        match[2]: dN where N is an integer, or None
        """
        # Get out if this roll's format isn't valid
        for match in matches:
            if not (match[0] == '+' or match[0] == '-' or not match[0]):
                return None
            if not (match[1].isnumeric() or '#' in match[1]):
                return None
            if not (not match[2] or match[2].lower().startswith('d')):
                return None
        try:
            overall_roll_breakdown = []
            total = 0
            for match in matches:
                subtotal = 0
                rolls = 1
                if match[2]:
                    # This is an actual roll (or group of rolls)
                    if '#' in match[1]:
                        amounts = match[1].split("#")
                        rolls = int(float(amounts[0]))
                        num_dice = int(float(amounts[1]))
                    else:
                        num_dice = int(float(match[1]))
                    size_dice = int(float(match[2][1:]))
                    for roll in range(0, rolls):
                        results = []
                        for i in range(0, num_dice):
                            roll = random.randint(1, size_dice)
                            results.append(roll)
                            if match[0] == "-":
                                subtotal -= roll
                            else:
                                subtotal += roll
                        roll_text = "%id%i" % (num_dice, size_dice)
                        results_string = roll_text + "=" + str(results)[1:][:-1]
                        overall_roll_breakdown.append(results_string)
                else:
                    # This is just adding or subtracting a number
                    if match[0] == "-":
                        subtotal -= int(float(match[1]))
                    else:
                        subtotal += int(float(match[1]))
                total += subtotal
            if message and not message.isspace():
                message = "%s, %s" % (nickname, message)
            else:
                message = nickname
            breakdown_string = "; ".join(overall_roll_breakdown)
            new_message = "%s: %i [%s]" % (message, total, breakdown_string)
            new_message = " ".join(new_message.split())
            if len(new_message.encode('utf-8')) > 512:
                # Just return the simplified result to get around IRC's limit
                new_message = "%s: %i" % (message, total)
            return new_message
        except Exception as e:
            print(e)
            return None


if __name__ == '__main__':
    with open('config.json', 'r') as config:
        config_dict = json.load(config)
        bot = PyIRCDiceBot(
            config_dict["nickname"],
            config_dict["realname"],
            config_dict["server"],
            config_dict["port"],
            config_dict["password"],
            config_dict["default_channels"]
        )

    bot.start()
