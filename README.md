# PyIRCDice

PyIRCDice is a script for running a dice-rolling bot in IRC.

## Usage

Requires the [IRC Package](https://python-irc.readthedocs.io/en/latest/index.html) for Python 3.

Edit the config.json file with the desired nickname for the bot and default channel(s). Separate each channel with a comma.

Execute ircdice.py with Python 3.

## Commands

"join <channelname>" or "cometo <channelname>" : Summon the bot to a channel.

"leave <channelname>" or "goaway <channelname>" : Bot leaves the specified channel.

"name <newname>" or "nick <newname>" : Changes the bot's nickname.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)
